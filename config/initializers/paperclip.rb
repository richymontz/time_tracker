Paperclip.interpolates(:user_placehold_it) do |attachment, style|
  # assumes using dimensions specified as integer, eg "250x250#"
  params = attachment.styles[style][:geometry].match(/\d+x\d+/).to_s + "&text=#{attachment.instance.name}"
  "http://www.placehold.it/" + params
end
