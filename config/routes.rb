Rails.application.routes.draw do
  resources :projects
  resources :task_times do
    member do
      get :continue_task
    end
  end
  get 'oauth_callbacks/twitter'
  devise_for :users, :controllers => { :omniauth_callbacks => "oauth_callbacks" }
  root 'task_times#index'
end
