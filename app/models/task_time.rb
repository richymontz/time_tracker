class TaskTime < ActiveRecord::Base
  belongs_to :project
  before_save :today

  def time
    diff = self.end_time.to_time - self.init_time.to_time
    '%d:%02d:%02d' % [ diff / 3600, (diff / 60) % 60, diff % 60 ]
  end

  def today
    self.task_date = DateTime.now if self.task_date.blank?
  end
end
