class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
   devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable, :omniauth_providers => [:twitter]

  has_attached_file :avatar,
   styles: { medium: "256x256>", profile: "128x128>", small: "64x64", icon: "64x64" }, default_url: ":user_placehold_it"

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

   def self.oauth(auth)
     where(provider: auth.provider, user_id: auth.uid).first_or_create do |user|
       user.provider = auth.provider #social network provider default: Twitter
       user.user_id = auth.uid
       user.name = auth.info.name
       user.avatar_from_url auth.info.image
       user.email = auth.info.email
       user.password = Devise.friendly_token[0,20]
     end
   end

   def avatar_from_url url
     self.avatar = open(url)
   end
end
