var sec = 0;
var timer = null;
var initTime = null;

function init(){
  sec = 0;
  timer = null;
  initTime = null;
}

function toggleTimer(elem){
  var object = $(elem)
  var pressed = object.attr('aria-pressed')
  if (pressed == "true") {
    object.text('Start')
    stop()
    endTime = new Date(initTime.getTime() + (sec * 1000))
    var data = {
      task_time:{
        name:       $("#name").val(),
        init_time:  initTime,
        end_time:   endTime,
        project_id: $("#project_id").val(),
      }
    }
    insertTime(data)
    sec = 0
  }else {
    object.text('Stop')
    initTime = new Date()
    start()
  }
}

function pad(val) {
    return val > 9 ? val : "0" + val;
}

function start(){
  timer = setInterval(function () {
    $('#timeCount').html(
      pad(parseInt(sec / 60, 10)) + ":" + pad(++sec % 60)
    )
  }, 1000);
}

function stop(){
  setTimeout(function () {
      clearInterval(timer);
  });
}

function insertTime(data){
  $.ajax({
    method: "POST",
    url: "/task_times",
    data: data
  })
}
