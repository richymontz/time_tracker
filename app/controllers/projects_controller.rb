class ProjectsController < ApplicationController
  layout 'admin'
  before_action :load_project, only: [:edit, :destroy, :update]


  def index
    @projects = Project.all.page(params[:page]).per(20)
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    if @project.save
      flash[:success] = "Project Created successfully"
    else
      flash[:success] = "Errors creating project: #{@project.errors.messages.to_sentence}"
    end
    redirect_to action: :index
  end

  def edit
  end

  def update
    if @project.update_attributes(project_params)
      flash[:success] = "Project Updated successfully"
    else
      flash[:success] = "Errors Updating project: #{@project.errors.messages.to_sentence}"
    end
    redirect_to action: :index
  end

  def destroy
    if @project.delete
      flash[:success] = "Project Deleted successfully"
    else
      flash[:success] = "Errors Deleting project: #{@project.errors.messages.to_sentence}"
    end
    redirect_to action: :index
  end

  private
    def load_project
      @project = Project.find_by(id: params[:id])
    end

    def project_params
      params.require(:project).permit(:name)
    end
end
