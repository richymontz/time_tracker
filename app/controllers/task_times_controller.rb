class TaskTimesController < ApplicationController
  layout 'admin'
  before_action :load_task_time, only: [:edit, :destroy, :update, :continue_task]
  before_action :load_projects, only: [:index,:edit, :new]


  def index
    @q = TaskTime.ransack(params[:q])
    @task_times = @q.result.includes(:project).order("created_at DESC").page(params[:page])
  end

  def new
    @task_time = TaskTime.new
  end

  def create
    @task_time = TaskTime.new(task_time_params)
    if @task_time.save
      flash[:success] = "Time Created successfully"
    else
      flash[:error] = "Errors creating task_time: #{@task_time.errors.messages.to_sentence}"
    end
    respond_to do |format|
      format.js {  }
      format.html {

        redirect_to action: :index
      }
    end
  end

  def edit
  end

  def update
    if @task_time.update_attributes(task_time_params)
      flash[:success] = "Time Updated successfully"
    else
      flash[:success] = "Errors Updating time: #{@task_time.errors.messages.to_sentence}"
    end
    redirect_to action: :index
  end

  def destroy
    if @task_time.delete
      flash[:success] = "Time Deleted successfully"
    else
      flash[:success] = "Errors Deleting time: #{@task_time.errors.messages.to_sentence}"
    end
    redirect_to action: :index
  end

  def continue_task
    respond_to do |format|
      format.js {  }
    end
  end

  private
    def load_task_time
      @task_time = TaskTime.find_by(id: params[:id])
    end

    def task_time_params
      params.require(:task_time).permit(:name, :init_time, :end_time, :project_id)
    end

    def load_projects
      @projects = Project.all
    end
end
