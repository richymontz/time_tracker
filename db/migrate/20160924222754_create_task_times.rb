class CreateTaskTimes < ActiveRecord::Migration
  def change
    create_table :task_times do |t|
      t.string :name
      t.datetime :init_time
      t.datetime :end_time
      t.references :project, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
