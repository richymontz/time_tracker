class AddTaskDateToTaskTimes < ActiveRecord::Migration
  def change
    add_column :task_times, :task_date, :date
  end
end
